from requests import get
from re import search
from os import environ
import json

def add_song(bot, channel, msg):
    url_pattern = "(?P<url>https?://[^\s]+)"
    link = dict(song = search(url_pattern, msg).group("url"))
    get(environ['QUEUE_URL'], params=link, verify=False)
    bot.msg(channel, f"Added {link['song']} to the queue.")

def current_song(bot, channel, msg):
    response = json.loads(get(environ['API_URL'] + "/previous").text)
    try:
        currentsong = response["PrevSongs"][0]
        bot.msg(channel, f"Playing now: {currentsong}")
    except IndexError:
        bot.msg(channel, "No song is currently playing.")

def get_streaming_url(bot, channel, msg):
    bot.msg(channel, f"Streaming on {environ['RTMP_URL']}")
