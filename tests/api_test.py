import unittest
import requests
import json

class testAPI(unittest.TestCase):
    def setUp(self):
        self.address = "http://0.0.0.0:8080"

    def testHealthy(self):
        response = requests.get(self.address + "/api/stat")
        self.assertEqual(response.status_code,200)

    def testQueueLen(self):
        response1 = requests.get(self.address + "/api/stat")
        requests.get(self.address + "/api/submit?song=https://www.youtube.com/watch?v=xMVTKOoy1uk")
        response2 = requests.get(self.address + "/api/stat")
        self.assertNotEqual(response1.text,response2.text)

    # def testQueueContents(self):
    #     requests.get("http://" + self.address + "/api/submit?song=https://www.youtube.com/watch?v=xMVTKOoy1uk")
    #     response = requests.get(self.address + "/api/queue")

        
if __name__ == '__main__':
    unittest.main()
